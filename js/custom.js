$('.selected').on('click', function() {
    $(this).next().toggleClass('opened');
    $('.select-language-list .arrow').toggleClass('opened');
    
  });
  
  
  $('.items > div').on('click', function() {
    var txt = $(this).find('.content > div:first-child b').text();
    var img = $(this).find('.img img')[0].currentSrc;

    var selectedLan = $(this).parent().parent().parent().find('.selected__lan');
    var selectedImg = $(this).parent().parent().parent().find('.selected__img img');
    if (txt==="England"){
      selectedLan.html("ENG");
    } else if (txt==="Vietnamese"){
      selectedLan.html("VI");
    } else {
      selectedLan.html("JP");
    }
    console.log(selectedImg)
    selectedImg[0].src = img;
   
    $(this).parent().parent().parent().find('.opened').removeClass('opened');
  });